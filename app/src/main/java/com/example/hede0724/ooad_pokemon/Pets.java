package com.example.hede0724.ooad_pokemon;

import java.util.ArrayList;

public class Pets
{
    private ArrayList<Pet> mypets;

    public Pets()
    {
        mypets= new ArrayList<Pet>();
    }

    public boolean AddPet(Pet newpet)
    {
        return this.mypets.add(newpet);
    }

    public void DeletePet(int index)
    {
        this.mypets.remove(index);
    }

    public void DisplayAllPets()
    {
        for(int i=0;i<this.mypets.size();i++)
        {
            this.mypets.get(i).Display();
        }
    }

    public int Size()
    {
        return this.mypets.size();
    }

}
