package com.example.hede0724.ooad_pokemon;

/**
 * Created by CatLin on 2016/6/11.
 */
public class Status
{
    private int mood;
    private int health;
    //private String pic;
    public Status(int mood, int health)
    {
        this.mood = mood;
        this.health = health;
    }
    public void Display()
    {
        System.out.printf("<Status> Mood:%d  Health:%d\n",this.mood,this.health);
    }
    public void Judge()
    {

    }

}
